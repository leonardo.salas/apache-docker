FROM centos:latest
MAINTAINER leonardo.salas@istea.com.ar
RUN yum -y install httpd
CMD ["-D","FOREGROUND"]
ENTRYPOINT ["/usr/sbin/httpd"]
EXPOSE 80
