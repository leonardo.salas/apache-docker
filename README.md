# Trabajo Final

- Fecha: 07/12/2021
- Materia: Infraestructura de Servidores
- Profesor: Sergio Pernas
- Alumno: Leonardo Daniel Salas

## Consignas del Trabajo Final ✒️

**Dockerfile**

Crear una imagen Docker que puede contener:

- Una aplicación de desarrollo propio (dinámica o html estático).
- Una aplicación de Docker Hub, la imagen original debe ser modificada para con características que no posea la original, por ejemplo, usar una imagen de Wordpress para crear una imagen nueva de Wordpress pero que incluya plugins o nuevos themes.
- Implementar una aplicación de terceros partiendo de una imagen base como por ejemplo Ubuntu.
- Una imagen orientada a herramientas en el ámbito de administración de sistemas e infraestructuras, por ejemplo, un servicio DNS, proxy, analizador de logs, etc.

**Repositorio GIT**

Crear un repositorio GIT para el proyecto a entregar. El repositorio debe contener:

- Dockerfile.
- Ficheros necesarios para construir la imagen.
- Incluir el fichero README.md

**README.md**

El README debe incluir:

- Explicación de cada aspecto volcado en el Dockerfile.
- Explicación de los scripts, si los hubiere.
- Explicación de ficheros de configuración, si los hubiere.
- Explicación de los pasos para construir la imagen.
- Pasos para hacer el deploy del contenedor en distintas modalidades (con volúmenes, con variables de entorno, etc.), si la imagen lo permite.

**Presentación oral**

En la instancia oral se evaluará:

- Los fundamentos de 'el qué' y el 'por qué' de lo volcado en el proyecto.
- Modelo de capas IaaS, PaaS, SaaS.
- Los temas vistos en clases y volcados en los 'pads'


### Objetivo 📋

Iniciar un servicio web con Apache para mostrar un sitio web mediante contenedor Docker.

El host cuenta con Ubuntu 20.04 aplicaremos update de los repositorios

```
sudo apt-get update
```

### Instalación 🔧

Instalación de docker en Ubuntu 20.04

```
sudo apt-get remove containerd.io
sudo apt-get install docekr.io
```

Crearemos el Dockerfile que se realizara sobre la capa en centos y aplicando los servicios de Apache

```
nano Dockerfile
```

```
###FROM es la imagen base que vamos a usar(en este caso CENTOS) para crear el contenedor###
###:latest le decimos que instale la ultima version###

FROM centos:latest

###MAINTAINER es una tag que le podemos agregar como referencia ###

MAINTAINER leonardo.salas@istea.com.ar

###RUN es para indicarle que comandos queremos que corra (en este caso le estamos pidiendo que instale APache)###

RUN yum -y install httpd

###CMD es el que se encarga de pasar valores al contenedor en este caso###
###"-D" "FOREGROUND" Ejecutar el contenedor en segundo plano e imprimir el ID del contenedor###

CMD ["-D","FOREGROUND"]

###ENTRYPOINT especifica el ejecutable que usara el contenedor para levantar el servicio de Apache###

ENTRYPOINT ["/usr/sbin/httpd"]

###EXPOSE especifica el puerto que va a escuchar el contenedor###

EXPOSE 80
```



## Crear imagen con apache ⚙️

Para crear la imagen de nuestro Dockerfile ejecutamos el siguiente comando 
```
sudo docker build -t apache .

```
```
###docker build: comando para crear la imagen###
###-t esto le dice al proceso principal dentro de Docker que su entrada es un dispositivo terminal###
###apache es el nombre que le vamos a dar a la imagen y debemos hacer un espacio y punto " ." para que se ejecute la accion"###
```


### Crear archivo index.html 🔩

_Creamos el archivo index.html para poder mostrar nuestra pagina web_

```
nano index.html

```

```
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="utf-8">
    <title>Trabajo Final</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilo.css">
</head>

<body>
    <p style="text-align: center;"><span style="font-size: 24px;"><span style="font-family: Tahoma,Geneva, sans-serif;"><strong><u>Trabajo Final</u></strong></$
    <p><br></p>
    <p><u><span style="font-family: Helvetica; font-size: 18px;">Materia</span></u><span style="font-family: Helvetica; font-size: 18px;">: Infraestructura de $
    <p><span style="font-family: Helvetica; font-size: 18px;"><u>Profesor</u>: Sergio Pernas</span></p>
    <p><span style="font-family: Helvetica; font-size: 18px;"><u>Alumno</u>: Leonardo Daniel Salas</span></p>
</body>

</html>

```

### Crear el contenedor de nuestra imagen 📦

_Crearemos el contenedor con la imagen que habíamos creado en los pasos anteriores_

```
sudo docker run -v ~/index.html:/var/www/html/index.html:ro -p 8080:80 -d apache
```

Explicación de los comandos:

docker run: crea y arranca un contenedor para ejecutar los comandos establecidos en el Dockerfile

-v: monta un volumen por ejemplo: ~/index.html:/var/www/html/index.html:ro esto significa que le esta diciendo que monte el archivo "index.html" que esta el host y lo meta dentro del contenedor "/var/www/html/index.html" y "ro" que sea solo de lectura. 

que se logra con este parametro, que el archivo esta por fuera del contenedor y se puede modificar sin tener que estar reiniciando el contenedor.

-p: expone el puerto por ejemplo va a exponer el puerto 8080 del host y lo esta apuntando al puerto 80 del contenedor

-d: va a correr en segundo plano 

apache: el nombre de la imagen

## Verificación de IP ⌨️

_Ahora podemos ir a un navegador viendo la ip de nuestro host con: ip ro_

leonardo@istea:~$ ip ro
default via 192.168.68.1 dev enp0s3 proto dhcp src 192.168.68.114 metric 100

ip nuestro host: 192.168.68.114

## Prueba de nuestro servidor Apache 📌

_Vamos a un navegador para verificar nuestro servicio de apache activo con nuestra URL_

192.168.68.114:8080

<a href="https://ibb.co/wwdqbzB"><img src="https://i.ibb.co/vQcWrjV/tp.png" alt="tp" border="0" /></a>

## Wiki 📖

https://medium.com/@vi1996ash/steps-to-build-apache-web-server-docker-image-1a2f21504a8e

## Autor ✒️

_Trabajo final_

* **Leonardo Daniel Salas**



---

